package com.huyen.bt5.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.huyen.bt5.entities.User;

public interface UserRepository extends CrudRepository<User,Integer>,PagingAndSortingRepository<User, Integer>{
	List<User> findByName(String q);
}
