package com.huyen.bt5.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.huyen.bt5.entities.User;
import com.huyen.bt5.service.UserService;


@Controller
public class HelloWorldController {
	@Autowired
	private UserService userService;

	@RequestMapping(value="/",method=RequestMethod.GET)
	public String listUser(Pageable pageable,Model model){	
		List<User> users=userService.listAllByPage(pageable).getContent();
		model.addAttribute("users", users);
		return "list";
	}
}
