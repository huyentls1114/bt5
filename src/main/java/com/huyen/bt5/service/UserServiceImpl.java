package com.huyen.bt5.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.huyen.bt5.entities.User;
import com.huyen.bt5.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	public Iterable<User> findAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}
	
	@Override
	public Page<User> listAllByPage(Pageable pageable){
		return userRepository.findAll(pageable);
	}

	public List<User> search(String q) {
		// TODO Auto-generated method stub
		return userRepository.findByName(q);
	}

	public User findOne(int id) {
		// TODO Auto-generated method stub
		return userRepository.findOne(id);
	}

	public void save(User user) {
		// TODO Auto-generated method stub
		userRepository.save(user);
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		userRepository.delete(id);
	}
	
}
