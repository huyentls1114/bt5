package com.huyen.bt5.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.huyen.bt5.entities.User;

public interface UserService {
	Iterable<User> findAll();
	List<User> search(String q);
	User findOne(int page);
	void save(User user);
	void delete(int id);
	Page<User> listAllByPage(Pageable pageable);
}
