<%@ page contentType="text/html; charset=UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <table class="table table-bordered">
        <thead>
            <tr class="success">
                <th>User Id</th>
                <th>Name</th>
                <th>Adress</th>
                <th>Birthday</th>
                <th>Married</th>
                <th>Job</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${users}" var="user">
                <tr class="warning">
                    <td><c:out value="${user.userid}" /></td>
                    <td><c:out value="${user.name}"/></td>
                    <td><c:out value="${user.adress}" /></td>
                    <td><fmt:formatDate pattern="yyyy-MMM-dd" value="${user.birthday}" /></td>
                    <td><c:out value="${user.married}" /></td>
                    <td><c:out value="${user.job}" /></td>
                    <td><a href="editUser?userid=<c:out value="${user.userid}"/>">Update</a></td>
                    <td><a href="deleteUser?userid=<c:out value="${user.userid}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a class="btn btn btn btn-primary"><a href="newUser" style="color:white">Add User</a></a>
    <a type="button" class="btn btn btn-primary" href="${pageContext.request.contextPath}/logout" style="color:white"> Log out</a>
</body>
</html>