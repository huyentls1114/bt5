<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="/resources/css/style.css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css" />
<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.14/js/jquery.tablesorter.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.14/js/jquery.tablesorter.widgets.js"></script>
<script>
	$(document).ready(function() {
		$('table').tablesorter()
	});
</script>
<style type="text/css">
div {
	text-align: center;
	padding: 25px 25px;
}

#menu {
	text-align: left;
	padding: 25px 25px;
}
</style>
</head>
<body>
	<div>
		<div class="ui inverted menu" id="menu">
			<a class="active item"> Home </a> <a class="item"> Messages </a> <a
				class="item"> Friends </a>
		</div>
		<h1 class="ui header">Danh sách user</h1>
		<div id="table">
			<table class="ui selectable celled  table" id="myTable">
				<thead>
					<tr>
						<th>User Id</th>
						<th>Name</th>
						<th>Adress</th>
						<th>Birthday</th>
						<th>Married</th>
						<th>Job</th>
						<th colspan=2>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users}" var="user">
						<tr>
							<td><c:out value="${user.userid}" default='0' /></td>
							<td><c:out value="${user.name}" /></td>
							<td><c:out value="${user.adress}" /></td>
							<td><fmt:formatDate pattern="yyyy-MMM-dd"
									value="${user.birthday}" /></td>
							<td><c:out value="${user.married}" /></td>
							<td><c:out value="${user.job}" /></td>
							<td><a
								href="editUser?userid=<c:out value="${user.userid}"/>">Update</a></td>
							<td><a
								href="deleteUser?userid=<c:out value="${user.userid}"/>">Delete</a></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="8">
							<a href="newUser" style="color: white" class="ui left floated small primary labeled icon button">
								<i class="user icon"></i> Add
									User</a>
						</th>
					</tr>
				</tfoot>
			</table>

			<a class="ui right floated small primary labeled icon button"
				href="${pageContext.request.contextPath}/logout"
				style="color: white"> <i class="user icon"></i> Log out
			</a>
		</div>
		<div class="ui pagination menu">
			<c:forEach begin="1" end="3" var="p">
				<a class="item" 
					href="<c:url value="/" ><c:param name="page" value="${p}"/><c:param name="size" value="1"/>${p}</c:url>">${p}</a>
			</c:forEach>
		</div>
	</div>

</body>
</html>