<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
<style type="text/css">
body>.grid {
	height: 100%;
}

.image {
	margin-top: -100px;
}

.column {
	max-width: 450px;
}

body {
	padding: 100px;
}
</style>
</head>
<body >
	<div class="ui middle aligned center aligned grid" id="login-box">
		<div class="column">
			<h2 class="ui image header">
				<div class="content">
					<h1>Log-in to your account</h1>
				</div>
			</h2>
			<c:if test="${not empty error}">
				<div class="error">${error}</div>
			</c:if>
			<c:if test="${not empty msg}">
				<div class="msg">${msg}</div>
			</c:if>
			<c:url var="loginUrl" value="/login" />

			<form action="${loginUrl}" method="post" class="ui large form">
				<div class="ui stacked secondary  segment">
					<div class="field">
						<div class="ui left icon input">
							<i class="user icon"></i> <input type='text' name='username'
								placeholder="Username">
						</div>
					</div>
					<div class="field">
						<div class="ui left icon input">
							<i class="lock icon"></i><input type='password' name='password'
								placeholder="Password">
						</div>
					</div>
					<input class="ui fluid large teal submit button" name="submit"
						type="submit" value="submit" />
				</div>

				<div class="ui error message"></div>

			</form>
		</div>
	</div>
</body>
</html>